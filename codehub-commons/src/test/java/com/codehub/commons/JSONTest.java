package com.codehub.commons;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.codehub.commons.JSON;

public class JSONTest {

	@Test
	public void test() {

		Truck volvo = new Truck("Volvo FMX", 25_00_000);
		String jsonString = JSON.string(volvo);
		Truck volvoConverted = JSON.object(jsonString, Truck.class);
		assertEquals(volvo, volvoConverted);
	}

	@SuppressWarnings("unused")
	private static class Truck {
		private String name;
		private int cost;

		// dummy constructor for jackson
		public Truck() {
		}

		public Truck(String name, int cost) {
			super();
			this.name = name;
			this.cost = cost;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getCost() {
			return cost;
		}

		public void setCost(int cost) {
			this.cost = cost;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + cost;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Truck other = (Truck) obj;
			if (cost != other.cost)
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	}
}
