package com.codehub.commons;

import org.junit.Test;

import com.codehub.commons.ASYNC;

public class ASYNCTest {

	@Test
	public void test() {
		ASYNC.runAsync(() -> {
			System.out.println("This code run asynchronously");
		});
	}

}
