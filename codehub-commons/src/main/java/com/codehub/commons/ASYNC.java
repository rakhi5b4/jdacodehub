package com.codehub.commons;

import java.util.concurrent.CompletableFuture;

public class ASYNC {
	
	public static void runAsync(Runnable runnable){
		CompletableFuture.runAsync(runnable/*, SOMETHING.getPool()*/).whenComplete((r,t)->{
			if(t!=null){
				System.out.println("Error while executing runnable.");
				t.printStackTrace();
			}else{
				System.out.println("Completed successfully @"+System.currentTimeMillis());
			}
		});
	}
}
