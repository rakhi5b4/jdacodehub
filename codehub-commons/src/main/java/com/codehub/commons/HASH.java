package com.codehub.commons;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class HASH {
	public static String md5Hex(String inp) {
		return Base64.encodeBase64URLSafeString(DigestUtils.md5((inp!=null)?inp:""));
	}
	public static String sha1Hex(String inp) {
		return Base64.encodeBase64URLSafeString(DigestUtils.sha1((inp!=null)?inp:""));
	}
}
