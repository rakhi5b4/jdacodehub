# JDA Codehub


## Table of contents

- [About](#about)
- [Development Setup](#development-setup)

## About
## Cloud Url
http://52.40.156.210:8080/

## Development Setup
### Pre-Requisites
- Java 8
- Eclipse Mars/Luna
- Install MongoDB
  Please follow the instruction from here https://www.mongodb.org/downloads
- Install Git from https://git-scm.com/downloads
  
  	
### Code Checkout
1. Checkout JDACodeHub from https://gitlab.com/rakhi5b4/jdacodehub.git in to $GITHOME

```bash
  # cd $GITHOME
  # git clone https://gitlab.com/rakhi5b4/jdacodehub.git
```
```bash
  # cd to $GITHOME/jdacodehub 
```

Note: You can also do step 1 and 2 using your favourite git tools like SourceTree


Generate the Eclipse project files
```bash
  # ./gradlew cleanEclipse eclipse
```
Note: If you are a windows user please use gradlew.bat

Create a new workspace $DEVHOME/ws/jdacodehub_ws and import the projects.
- In Eclipse Mars go to File > Import… > General > Existing projects in to workspace. Ensure that "search for nested projects" check box is checked.

### Setting up database
cd to $GITHOME/jdacodehub/db -> Extract the submissions_mongodump.zip
If you can't find the zip please use the following url to download the zip:
https://drive.google.com/file/d/0B429UneQIpJfNkVQcFJhV1hqdHc/view?usp=sharing

1. Start the mongodb server and login to mongo console.
2. Create a database named "codehub"
	Syntax: use codehub
3. Create a collection named "submissions"   
4. Import the extracted json file from db folder i.e. submissions.json into submissions collection created in step 2 using the following statement:
    mongoimport -d codehub -c submissions --file $GITHOME/jdacodehub/db/submissions.json --jsonArray

### Running in local
After the setup please run the Main from com.codehub.Start 

### Build Process
Issue the below command to build all projects along with unit testing.
To build distribution
```bash
  # ./gradlew distZip
```
```bash
  # cd $GITHOME
  # ./gradlew build
```
To ignore tests. Invoke
```bash
  # ./gradlew build -x test
```

## Product Installation

### Pre-Install Requirements
Make sure that below softwares are present on box
- Java 8
- MongoDB
 
## Release Notes