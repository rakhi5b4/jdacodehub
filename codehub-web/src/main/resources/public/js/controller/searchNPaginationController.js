codehubApp.controller('searchNPaginationController', function($scope, $http) {
	pagination.redrawPageinationDiv();
	pagination.setHttpObj($http);
	pagination.registerPageClick();
	$scope.serachSubmissions = function() {
    	submissionWidgetPanel.reloadGadgets($http);    	
    }    
	$scope.previous = function() {
		pagination.decrementPageSet();
		pagination.redrawPageinationDiv();
	}
	$scope.next = function() {
		pagination.incrementPageSet();
		pagination.redrawPageinationDiv();
	}
 });
