codehubApp.controller('statisticsController', function($scope, $http) {
	var url = "http://"+hostName+":8080/submission/stats";
    
    $http.get(url).success( function(response) {
    	var languages = Object.keys(response.top_5_languages_used);
    	var top2SubmissionsAttempted = response.top_2_submissions_attempted;
    	$scope.stats = {
    			totalSubmissions: response.total_submissions,
    			easyCount: response.submissions_per_level['Easy'],
    			mediumCount: response.submissions_per_level['Medium'],
    			hardCount: response.submissions_per_level['Hard'],
	    };
    	if(languages[0]) {
    		$scope.stats.lang1 = languages[0];
    		$scope.stats.langSubmissionCount1= response.top_5_languages_used[languages[0]];
    	}
    	if(languages[1]) {
    		$scope.stats.lang2 = languages[1];
    		$scope.stats.langSubmissionCount2 = response.top_5_languages_used[languages[1]];
    	}
    	if(languages[2]) {
    		$scope.stats.lang3 = languages[2];
    		$scope.stats.langSubmissionCount3 = response.top_5_languages_used[languages[2]];
    	}
    	if(languages[3]) {
    		$scope.stats.lang4= languages[3];
    		$scope.stats.langSubmissionCount4 = response.top_5_languages_used[languages[3]];
    	}
    	if(languages[4]) {
    		$scope.stats.lang5= languages[4];
    		$scope.stats.langSubmissionCount5= response.top_5_languages_used[languages[4]];
    	}
    	
    	if(top2SubmissionsAttempted[0]) {
    		$scope.stats.submissionTitle1 = top2SubmissionsAttempted[0].name;
    		$scope.stats.attemptedCount1 = top2SubmissionsAttempted[0].count;
    	}
    	if(top2SubmissionsAttempted[1]) {
    		$scope.stats.submissionTitle2 = top2SubmissionsAttempted[1].name;
    		$scope.stats.attemptedCount2 = top2SubmissionsAttempted[1].count;
    	}
    });    
 });
