var pagination = {
	currMinPageLimit : 0,
	httpObj : null,
	setHttpObj : function(httpObj) {
		this.httpObj = httpObj;
	},
	redrawPageinationDiv : function() {
		//var paginationHtml = '<ul class="pagination" style="margin: 0px"><li class="disabled"><a href="javascript:void(0)" aria-label="Previous"><span	aria-hidden="true">«</span></a></li>';
						
		var i;
		for(i= 1; i<=10 ; i++) {
			$("#page"+i).html(this.currMinPageLimit+i);
			//paginationHtml = paginationHtml + '<li><a href="javascript:void(0)">'+i+'</a></li>';
		}
		//paginationHtml = paginationHtml + '<li><a href="javascript:void(0)" aria-label="Next"><span aria-hidden="true">»</span></a></li></ul>';
		//this.resetPaginationDiv();
		//$("#paginationRoot").append(paginationHtml);
		this.resetPreviousNext();
	},
	resetPreviousNext : function() {
		if(this.currMinPageLimit == 0) {
			$("#previousPageSet").addClass("disabled");
			//$("#nextPageSet").removeClass("disabled");
		} else {
			//$("#nextPageSet").addClass("disabled");
			$("#previousPageSet").removeClass("disabled");
		}
	},
	incrementPageSet : function() {
		$("li[name*=pageEleParent]").removeClass("active");
		this.currMinPageLimit = this.currMinPageLimit + 10;
	},
	decrementPageSet : function() {
		$("li[name*=pageEleParent]").removeClass("active");
		this.currMinPageLimit = (this.currMinPageLimit - 10) > -1 ? (this.currMinPageLimit-10) : 0;	
	},
	registerPageClick : function() {
		var me = this;
		$("a[name*='pageEle']").click(function(){
			
			if(!isNaN(this.id.replace("page", ""))) {
				var currPageRoot = parseInt(this.id.replace("page", ""));						
				currentPage = currPageRoot + me.currMinPageLimit;
				submissionWidgetPanel.reloadGadgets(me.httpObj);    
				$("li[name*=pageEleParent]").removeClass("active");
				$("#pageEleParent"+currPageRoot).addClass("active");
			}
		});
	}
};