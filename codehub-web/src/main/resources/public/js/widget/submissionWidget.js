var submissionWidgetPanel = {
	reloadGadgets: function($http) {
		var url = "http://"+hostName+":8080/submission";
		var searchStr = $("#searchSubmissions").val();
		var isSearchSet = false;
		if(searchStr != null && searchStr.trim() != "") {
			url = url + "?searchStr="+encodeURIComponent(searchStr);
			isSearchSet = true;
		}
		var statusStr = this.createStatusString();
		var isStatusSet = false;
		if(statusStr != null && statusStr.trim() != "") {
			var _prefix = isSearchSet ? "&" : "?";
			url = url + _prefix+"status="+encodeURIComponent(statusStr);
			isStatusSet = true;
		}
		if(currentPage) {
			var _prefix = (isSearchSet || isStatusSet) ? "&" : "?";
			url = url + _prefix+"page="+encodeURIComponent(currentPage);
		}
	    $http.get(url).success( function(response) {
	    	submissionWidgetPanel.resetGadgets();
	    	submissionWidgetPanel.redrawGadgets(response);
	    	submissionWidgetPanel.highlightCode();
	    });    
	    
	},
	
	createStatusString : function() {
		var statusStr = "";
		$("input[name*='statusCheckBox']").each(function(){
			if(this.checked) {
				statusStr = statusStr + statusMap[this.id] + "#";
			}			   
		});	
		return statusStr.substring(0, statusStr.length -1);
	},
	
	redrawGadgets: function(data) {
		var i =0;
		var screenWidth = window.screen.availWidth;
		var _suffix = 12;
		if(screenWidth > 1600) {
			_suffix = 6;
		}
		for(i=0;i<data.length;i++) {
			var htmlContent = '<div class="col-lg-'+_suffix+'">'+
								'<div class="panel panel-default">'+
									'<div class="panel-heading">'+
										'<i class="fa fa-file-code-o fa-fw"></i><span> '+data[i].title+
										'</span><div class="btn-group btn-group-xs" role="group" style="float: right;">'+
											'<button type="button" class="btn btn-default btn-info">'+data[i].status+'</button>'+
											'<button type="button" class="btn btn-default btn-warning">'+data[i].language+'</button>'+
											'<button type="button" class="btn btn-default btn-primary">'+data[i].metadata.level+'</button>'+
										'</div>'+
									'</div>'+
									'<div class="panel-body code-div"><pre><code>'+data[i].source+'</code></pre></div>'+
								'</div>'+
							'</div>';
			$("#submissionGadgetsDiv").append(htmlContent);
		}
		
	},
	
	resetGadgets: function() {
		$("#submissionGadgetsDiv").empty();
	},
	
	highlightCode: function() {
		$(document).ready(function() {
		  $('pre code').each(function(i, block) {
		    hljs.highlightBlock(block);
		  });
		});
	}
}