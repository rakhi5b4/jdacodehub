package com.codehub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class Start {
	public static void main(String[] args) {
		SpringApplication.run(Start.class, args);
	}
}
