package com.codehub.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.codehub.model.Submission;

public interface SubmissionRepository extends MongoRepository<Submission, String> {
	List<Submission> findByTitle(String title);

	List<Submission> findByStatus(String phone);
}
