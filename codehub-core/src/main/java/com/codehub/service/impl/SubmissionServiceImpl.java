package com.codehub.service.impl;

// imports as static
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.codehub.model.LevelSubmissions;
import com.codehub.model.Statistics;
import com.codehub.model.Submission;
import com.codehub.model.TopLanguage;
import com.codehub.repository.SubmissionRepository;
import com.codehub.service.SubmissionService;

@Service
public class SubmissionServiceImpl implements SubmissionService {
	private static final Logger logger = LoggerFactory.getLogger(SubmissionServiceImpl.class);
	@Value("${submissions.topLanguages.count}") private int topLanguagesCount;
	@Value("${submissions.topSubmissionsAttempted.count}") private int topSubmissionsAttempted;
	@Value("${status.delimiter}") private String delimter;
	@Value("${pageLimit}") private int pageLimit;

	@Autowired SubmissionRepository submissionRepository;

	@Autowired MongoTemplate mongoTemplate;

	@SuppressWarnings("static-access")
	@Override
	public List<Submission> read(String searchStr, String status, int pageNo) {

		List<Criteria> criterias = new ArrayList<Criteria>();

		List<Criteria> searchCriterias = new ArrayList<Criteria>();
		List<Criteria> statusCriterias = new ArrayList<Criteria>();
		Criteria c = new Criteria();
		Criteria searchCriteria = new Criteria();
		Criteria statusCriteria = new Criteria();

		if (searchStr != null) {
			searchCriterias.add(Criteria.where("title").regex(Pattern.compile(searchStr, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));
			searchCriterias.add(Criteria.where("language").regex(Pattern.compile(searchStr, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));
			searchCriterias.add(Criteria.where("metadata.level").regex(
					Pattern.compile(searchStr, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));

			searchCriteria.orOperator(searchCriterias.toArray(new Criteria[searchCriterias.size()]));
		}
		if (status != null) {
			String statuses[] = status.split(delimter);
			for (String statusStr : statuses) {
				statusCriterias.add(Criteria.where("status").regex(Pattern.compile(statusStr, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE)));
			}
			statusCriteria.orOperator(statusCriterias.toArray(new Criteria[statusCriterias.size()]));
		}
		if (searchCriterias.size() > 0 || statusCriterias.size() > 0) {
			c.andOperator(searchCriteria, statusCriteria);
		}
		Query query = new Query();
		query.addCriteria(c);
		int skipLimit = (pageNo - 1) * pageLimit;
		query.skip(skipLimit);
		query.limit(pageLimit);
		return mongoTemplate.find(query, Submission.class);
	}

	@Override
	public Statistics readStats() {
		Statistics stats = new Statistics();
		Aggregation aggr = newAggregation(group("language").count().as("total"), project("total").and("language").previousOperation(),
				sort(Sort.Direction.DESC, "total"), skip(0), limit(topLanguagesCount));
		// Convert the aggregation result into a List
		AggregationResults<TopLanguage> groupResults = mongoTemplate.aggregate(aggr, Submission.class, TopLanguage.class);
		List<TopLanguage> topLanguagesList = groupResults.getMappedResults();

		topLanguagesList.forEach(langObj -> {
			stats.addToTop5LanguagesUsed(langObj.getLanguage(), langObj.getTotal());
		});
		stats.setTotal_submissions(submissionRepository.count());

		Aggregation levelAggr = newAggregation(group("metadata.level").count().as("total"), project("total").and("metadata.level")
				.previousOperation(), sort(Sort.Direction.DESC, "total"));
		AggregationResults<LevelSubmissions> levelResults = mongoTemplate.aggregate(levelAggr, Submission.class, LevelSubmissions.class);
		List<LevelSubmissions> levelSubmissionsList = levelResults.getMappedResults();

		levelSubmissionsList.forEach(levelData -> {
			stats.addSubmissionPerLevel(levelData.getMetadata().getLevel(), levelData.getTotal());
		});

		Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "metadata.users_attempted"));
		query.limit(topSubmissionsAttempted);
		List<Submission> topSubmissionsAttemptedList = mongoTemplate.find(query, Submission.class);
		topSubmissionsAttemptedList.forEach(submissionObj -> {
			stats.addToTop2SubmissionsAttempted(submissionObj.getTitle(), submissionObj.getMetadata().getUsers_attempted());
		});
		return stats;
	}
}
