package com.codehub.service;

import java.util.List;

import com.codehub.model.Statistics;
import com.codehub.model.Submission;

public interface SubmissionService {
	public List<Submission> read(String searchStr, String status, int pageNo);

	public Statistics readStats();

}
