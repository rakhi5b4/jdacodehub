package com.codehub.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Statistics {

	private Map<String, Long> top_5_languages_used;
	private List<SubmissionAttempted> top_2_submissions_attempted;
	private Map<String, Long> submissions_per_level;
	private long total_submissions;

	public Map<String, Long> getTop_5_languages_used() {
		return top_5_languages_used;
	}

	public void setTop_5_languages_used(Map<String, Long> top_5_languages_used) {
		this.top_5_languages_used = top_5_languages_used;
	}

	public void addToTop5LanguagesUsed(String language, long submissionCount) {
		if (top_5_languages_used == null) {
			top_5_languages_used = new HashMap<String, Long>();
		}
		top_5_languages_used.put(language, submissionCount);
	}

	public List<SubmissionAttempted> getTop_2_submissions_attempted() {
		return top_2_submissions_attempted;
	}

	public void setTop_2_submissions_attempted(List<SubmissionAttempted> top_2_submissions_attempted) {
		this.top_2_submissions_attempted = top_2_submissions_attempted;
	}

	public void addToTop2SubmissionsAttempted(String name, long occurrenceCount) {
		if (top_2_submissions_attempted == null) {
			top_2_submissions_attempted = new ArrayList<SubmissionAttempted>();
		}
		top_2_submissions_attempted.add(new SubmissionAttempted(name, occurrenceCount));
	}

	public Map<String, Long> getSubmissions_per_level() {
		return submissions_per_level;
	}

	public void setSubmissions_per_level(Map<String, Long> submissions_per_level) {
		this.submissions_per_level = submissions_per_level;
	}

	public void addSubmissionPerLevel(String level, long submissionCount) {
		if (submissions_per_level == null) {
			submissions_per_level = new HashMap<String, Long>();
		}
		submissions_per_level.put(level, submissionCount);
	}

	public long getTotal_submissions() {
		return total_submissions;
	}

	public void setTotal_submissions(long total_submissions) {
		this.total_submissions = total_submissions;
	}
}
