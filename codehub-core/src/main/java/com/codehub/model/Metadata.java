package com.codehub.model;

public class Metadata {
	private String level;
	private int rating;
	private int users_attempted;

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getUsers_attempted() {
		return users_attempted;
	}

	public void setUsers_attempted(int users_attempted) {
		this.users_attempted = users_attempted;
	}
}
