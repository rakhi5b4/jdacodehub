package com.codehub.controller;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codehub.model.Statistics;
import com.codehub.model.Submission;
import com.codehub.service.SubmissionService;

@Api(name = "Submission API", description = "APIs for managing code submissions")
@RestController
@RequestMapping("/submission")
public class SubmissionController {
	private static final Logger logger = LoggerFactory.getLogger(SubmissionController.class);
	private static final String USER_ID = "userId";
	@Autowired SubmissionService submissionService;

	@ApiMethod
	@RequestMapping(method = RequestMethod.GET)
	public List<Submission> readAll(@RequestParam(value = "searchStr", required = false) String searchStr, @RequestParam(value = "status",
			required = false) String status, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		return submissionService.read(searchStr, status, page);
	}

	/* @ApiMethod
	 * 
	 * @RequestMapping(value = "/{id}", method = RequestMethod.GET) public Submission read(@ApiPathParam(name = "id") @PathVariable String id) { return submissionService.read(id); } */
	@ApiMethod
	@RequestMapping(value = "/stats", method = RequestMethod.GET)
	public Statistics readStats() {
		return submissionService.readStats();
	}
}